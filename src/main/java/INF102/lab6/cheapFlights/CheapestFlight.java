package INF102.lab6.cheapFlights;

import java.util.List;

import java.util.Objects;
import java.util.Comparator;
import java.util.Set;
import java.util.HashSet;
import java.util.PriorityQueue;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {

    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();

        Set<City> addedVertices = new HashSet<>(); // Keep track of added vertices

        for (Flight flight : flights) {
            City startCity = flight.start;
            City destinationCity = flight.destination;
            int cost = flight.cost;

            // Add vertices if they don't exist
            if (!addedVertices.contains(startCity)) {
                graph.addVertex(startCity);
                addedVertices.add(startCity);
            }
            if (!addedVertices.contains(destinationCity)) {
                graph.addVertex(destinationCity);
                addedVertices.add(destinationCity);
            }

            // Add directed edge from source to destination with the cost as the weight
            graph.addEdge(startCity, destinationCity, cost);
        }

        return graph;
    }


    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);

        PriorityQueue<CityAndStops> queue = new PriorityQueue<>(Comparator.comparingInt(cs -> cs.cost));
        Set<CityAndStops> visited = new HashSet<>();

        queue.add(new CityAndStops(start, 0, 0));

        while (!queue.isEmpty()) {
            CityAndStops currentCityAndStops = queue.poll();
            City currentCity = currentCityAndStops.city;
            int stops = currentCityAndStops.stops;
            int cost = currentCityAndStops.cost;

            if (currentCity.equals(destination) && stops <= nMaxStops+1) {
                return cost;
            }

            visited.add(currentCityAndStops);

            for (City neighbor : graph.outNeighbours(currentCity)) {
                int newCost = cost + graph.getWeight(currentCity, neighbor);

                CityAndStops nextCityAndStops = new CityAndStops(neighbor, stops + 1, newCost);

                if (!visited.contains(nextCityAndStops) && stops + 1 <= nMaxStops+1) {
                    queue.add(nextCityAndStops);
                }
            }
        }

        return -1;
    }

    private static class CityAndStops {
        private final City city;
        private final int stops;
        private final int cost;

        private CityAndStops(City city, int stops, int cost) {
            this.city = city;
            this.stops = stops;
            this.cost = cost;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CityAndStops that = (CityAndStops) o;
            return stops == that.stops && cost == that.cost && Objects.equals(city, that.city);
        }

        @Override
        public int hashCode() {
            return Objects.hash(city, stops, cost);
        }
    }

    
}